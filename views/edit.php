<!DOCTYPE html>
<html lang="en">
<head>
    <title>Administácia výletov</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
        rel="shortcut icon" 
        type="image/jpg"  
        href="img/favicon.png"
    >
    <link 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
        crossorigin="anonymous"
    >
    <link 
        href="/css/edit.css" 
        rel="stylesheet" 
    >
    <?php if(isset($_GET['vylet'])) { ?>
        <style>
            .trip {
                display:none;
            }
        </style>
    <?php } ?>
</head>
<body>
    <a href="/">
        <div class="map-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-globe-americas" viewBox="0 0 16 16">
                <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0ZM2.04 4.326c.325 1.329 2.532 2.54 3.717 3.19.48.263.793.434.743.484-.08.08-.162.158-.242.234-.416.396-.787.749-.758 1.266.035.634.618.824 1.214 1.017.577.188 1.168.38 1.286.983.082.417-.075.988-.22 1.52-.215.782-.406 1.48.22 1.48 1.5-.5 3.798-3.186 4-5 .138-1.243-2-2-3.5-2.5-.478-.16-.755.081-.99.284-.172.15-.322.279-.51.216-.445-.148-2.5-2-1.5-2.5.78-.39.952-.171 1.227.182.078.099.163.208.273.318.609.304.662-.132.723-.633.039-.322.081-.671.277-.867.434-.434 1.265-.791 2.028-1.12.712-.306 1.365-.587 1.579-.88A7 7 0 1 1 2.04 4.327Z"/>
            </svg>
        </div>
    </a>
    <nav class="border-bottom bg-white pt-3 pb-3">
        <div class="container">
            <b>Rodinné výlety</b> <div class="d-none d-md-inline">/ Administrácia výletov</div>
            <div class="float-end">
				<a href="https://www.photopea.com" class="me-3 text-decoration-none text-black" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Editor fotiek (1280x720)">
					<svg width="16px" height="16px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>resize_round [#836]</title>
						<desc>Created with Sketch.</desc>
						<defs>
						</defs>
							<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="Dribbble-Light-Preview" transform="translate(-220.000000, -4479.000000)" fill="#000000">
									<g id="icons" transform="translate(56.000000, 160.000000)">
										<path d="M181,4337 L167,4337 C166.448,4337 166,4336.552 166,4336 L166,4322 C166,4321.448 166.448,4321 167,4321 L173,4321 C173.552,4321 174,4321.448 174,4322 L174,4327 C174,4328.105 174.895,4329 176,4329 L181,4329 C181.552,4329 182,4329.448 182,4330 L182,4336 C182,4336.552 181.552,4337 181,4337 L181,4337 Z M174,4319 L166,4319 C164.895,4319 164,4319.895 164,4321 L164,4337 C164,4338.105 164.895,4339 166,4339 L182,4339 C183.105,4339 184,4338.105 184,4337 L184,4329 L184,4321 C184,4319.895 183.105,4319 182,4319 L174,4319 Z" id="resize_round-[#836]">

										</path>
								</g>
							</g>
						</g>
					</svg>
				</a>
				<a href="https://imagecompressor.com/" class="me-3 text-black text-decoration-none" target="blank" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Optimalizácia veľkosti fotky">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-droplet-half" viewBox="0 0 16 16">
					  <path fill-rule="evenodd" d="M7.21.8C7.69.295 8 0 8 0c.109.363.234.708.371 1.038.812 1.946 2.073 3.35 3.197 4.6C12.878 7.096 14 8.345 14 10a6 6 0 0 1-12 0C2 6.668 5.58 2.517 7.21.8zm.413 1.021A31.25 31.25 0 0 0 5.794 3.99c-.726.95-1.436 2.008-1.96 3.07C3.304 8.133 3 9.138 3 10c0 0 2.5 1.5 5 .5s5-.5 5-.5c0-1.201-.796-2.157-2.181-3.7l-.03-.032C9.75 5.11 8.5 3.72 7.623 1.82z"/>
					  <path fill-rule="evenodd" d="M4.553 7.776c.82-1.641 1.717-2.753 2.093-3.13l.708.708c-.29.29-1.128 1.311-1.907 2.87l-.894-.448z"/>
					</svg>
				</a>
				<a href="https://eu.zonerama.com/" class="me-3 text-decoration-none text-black" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Fotoalbumy">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-images" viewBox="0 0 16 16">
					  <path d="M4.502 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
					  <path d="M14.002 13a2 2 0 0 1-2 2h-10a2 2 0 0 1-2-2V5A2 2 0 0 1 2 3a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v8a2 2 0 0 1-1.998 2zM14 2H4a1 1 0 0 0-1 1h9.002a2 2 0 0 1 2 2v7A1 1 0 0 0 15 11V3a1 1 0 0 0-1-1zM2.002 4a1 1 0 0 0-1 1v8l2.646-2.354a.5.5 0 0 1 .63-.062l2.66 1.773 3.71-3.71a.5.5 0 0 1 .577-.094l1.777 1.947V5a1 1 0 0 0-1-1h-10z"/>
					</svg>
				</a>
                <a href="https://slovenskycestovatel.sk/map" target="_blank" class="text-black text-decoration-none"  data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Kam na výlet?">
                    <svg style="margin-top:-2px;" width="16px" height="16px" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--gis" preserveAspectRatio="xMidYMid meet"><path d="M21 32C9.459 32 0 41.43 0 52.94c0 4.46 1.424 8.605 3.835 12.012l14.603 25.244c2.045 2.672 3.405 2.165 5.106-.14l16.106-27.41c.325-.59.58-1.216.803-1.856A20.668 20.668 0 0 0 42 52.94C42 41.43 32.544 32 21 32zm0 9.812c6.216 0 11.16 4.931 11.16 11.129c0 6.198-4.944 11.127-11.16 11.127c-6.215 0-11.16-4.93-11.16-11.127c0-6.198 4.945-11.129 11.16-11.129z" fill="#000000"></path><path d="M87.75 0C81.018 0 75.5 5.501 75.5 12.216c0 2.601.83 5.019 2.237 7.006l8.519 14.726c1.193 1.558 1.986 1.262 2.978-.082l9.395-15.99c.19-.343.339-.708.468-1.082a12.05 12.05 0 0 0 .903-4.578C100 5.5 94.484 0 87.75 0zm0 5.724c3.626 0 6.51 2.876 6.51 6.492c0 3.615-2.884 6.49-6.51 6.49c-3.625 0-6.51-2.875-6.51-6.49c0-3.616 2.885-6.492 6.51-6.492z" fill="#000000"></path><path d="M88.209 37.412c-2.247.05-4.5.145-6.757.312l.348 5.532a126.32 126.32 0 0 1 6.513-.303zm-11.975.82c-3.47.431-6.97 1.045-10.43 2.032l1.303 5.361c3.144-.896 6.402-1.475 9.711-1.886zM60.623 42.12a24.52 24.52 0 0 0-3.004 1.583l-.004.005l-.006.002c-1.375.866-2.824 1.965-4.007 3.562c-.857 1.157-1.558 2.62-1.722 4.35l5.095.565c.038-.406.246-.942.62-1.446h.002v-.002c.603-.816 1.507-1.557 2.582-2.235l.004-.002a19.64 19.64 0 0 1 2.388-1.256zM58 54.655l-3.303 4.235c.783.716 1.604 1.266 2.397 1.726l.01.005l.01.006c2.632 1.497 5.346 2.342 7.862 3.144l1.446-5.318c-2.515-.802-4.886-1.576-6.918-2.73c-.582-.338-1.092-.691-1.504-1.068zm13.335 5.294l-1.412 5.327l.668.208l.82.262c2.714.883 5.314 1.826 7.638 3.131l2.358-4.92c-2.81-1.579-5.727-2.611-8.538-3.525l-.008-.002l-.842-.269zm14.867 7.7l-3.623 3.92c.856.927 1.497 2.042 1.809 3.194l.002.006l.002.009c.372 1.345.373 2.927.082 4.525l5.024 1.072c.41-2.256.476-4.733-.198-7.178c-.587-2.162-1.707-4.04-3.098-5.548zM82.72 82.643a11.84 11.84 0 0 1-1.826 1.572h-.002c-1.8 1.266-3.888 2.22-6.106 3.04l1.654 5.244c2.426-.897 4.917-1.997 7.245-3.635l.006-.005l.003-.002a16.95 16.95 0 0 0 2.639-2.287zm-12.64 6.089c-3.213.864-6.497 1.522-9.821 2.08l.784 5.479c3.421-.575 6.856-1.262 10.27-2.18zm-14.822 2.836c-3.346.457-6.71.83-10.084 1.148l.442 5.522c3.426-.322 6.858-.701 10.285-1.17zm-15.155 1.583c-3.381.268-6.77.486-10.162.67l.256 5.536c3.425-.185 6.853-.406 10.28-.678zm-15.259.92c-2.033.095-4.071.173-6.114.245l.168 5.541a560.1 560.1 0 0 0 6.166-.246z" fill="#000000" fill-rule="evenodd"></path></svg>
                </a>
            </div>
        </div>
    </nav>
    <div class="container pt-4">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="card rounded-0 mb-3">
                    <div class="card-header p-3 bg-white border-bottom rounded-0 border-0">
                        Pridať nový výlet
                    </div>
                    <form action="/add" method="POST" enctype="multipart/form-data">
                        <div class="card-body p-3">
                            <div class="form-floating mb-3">
                                <input 
                                    name="name"
                                    type="text" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                                <label for="floatingInput">
                                    Názov výletu
                                </label>
                            </div>
                            <div class="form-floating mb-3">
                                <input 
                                    name="album"
                                    type="text" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                                <label for="floatingInput">
                                    Stránka fotoalbumu
                                </label>
                            </div>
                            <div class="form-floating mb-3">
                                <input 
                                    name="latitude"
                                    type="text" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                                <label for="floatingInput">
                                    Zemepisná šírka (lat)
                                </label>
                            </div>
                            <div class="form-floating mb-3">
                                <input 
                                    name="longitude"
                                    type="text" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                                <label for="floatingInput">
                                    Zemepisná dĺžka (long)
                                </label>
                            </div>
                            <div class="form-floating mb-3">
                                <input 
                                    name="date"
                                    type="date" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                                <label for="floatingInput">
                                    Dátum
                                </label>
                            </div>
                            <div class="bg-white border rounded p-2 mb-0">
                                <label for="floatingInput" class="image-upload-title">
                                    Titulná fotka
                                </label>
                                <input 
                                    name="image"
                                    type="file" 
                                    class="form-control" 
                                    id="floatingInput" 
                                    autocomplete="off"
                                    required
                                >
                            </div>
                        </div>
                        <div class="card-footer p-3 bg-white">
                            <button class="btn btn-success">
                                Pridať výlet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <?php if(is_array($locations) && count($locations)) { ?>
                <div class="col-md-6 col-lg-8 mb-3">
                    <div class="card p-3 rounded-0 border-bottom-0">
                        <div class="search-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                            </svg>
                        </div>
                        <input 
                            type="text" 
                            class="form-control" 
                            style="padding-left: 35px;"
                            placeholder="Hľadaj konkrétny výlet"
                            id="phrase"
                        >
                    </div>
                    <div class="card border-top-0 rounded-0">
                        <div class="card-body p-0">
                            <div class="accordion accordion-flush ">
                                <?php foreach($locations as $key => $location) { ?>
                                    <form action="/edit/<?= $key ?>" method="POST" enctype="multipart/form-data" class="trip" search="<?= strtolower($location->name) ?> <?= date("d.m.Y", strtotime($location->date)) ?>">
                                        <div class="accordion-item border-bottom-0">
                                            <h2 class="accordion-header">
                                                <button 
                                                    class="accordion-button collapsed bg-white box-shadown-none w-100" 
                                                    type="button" 
                                                    data-bs-toggle="collapse" 
                                                    data-bs-target="#flush-collapse-<?= $key ?>" 
                                                    aria-expanded="false" 
                                                    aria-controls="flush-collapseOne"
                                                >
                                                    <div class="row w-100">
                                                        <div class="col-3 col-sm-2 col-md-3 col-lg-2 col-xl-1">
                                                            <div class="bg-dark h-100 rounded" style="background-image: url('<?= $location->image ?>'); background-size: cover; background-position : center">
                                                            </div>
                                                        </div>
                                                        <div class="col-9 col-sm-10 col-md-9 col-lg-10 col-xl-11">
                                                            <?= $location->name ?>
                                                            <small class="d-block mt-1">
                                                                <?= date("d.m.Y", strtotime($location->date)) ?>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div 
                                                id="flush-collapse-<?= $key ?>" 
                                                class="accordion-collapse collapse" 
                                            >
                                                <div class="accordion-body border-top p-3">
                                                    <div class="form-floating mb-3">
                                                        <input 
                                                            name="name"
                                                            type="text" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            value="<?= $location->name ?>"
                                                            required
                                                        >
                                                        <label for="floatingInput">
                                                            Názov výletu
                                                        </label>
                                                    </div>
                                                    <div class="form-floating mb-3">
                                                        <input 
                                                            name="album"
                                                            type="text" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            value="<?= $location->album ?>"
                                                            required
                                                        >
                                                        <label for="floatingInput">
                                                            Stránka fotoalbumu
                                                        </label>
                                                    </div>
                                                    <div class="form-floating mb-3">
                                                        <input 
                                                            name="latitude"
                                                            type="text" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            value="<?= $location->lat ?>"
                                                            required
                                                        >
                                                        <label for="floatingInput">
                                                            Zemepisná šírka (lat)
                                                        </label>
                                                    </div>
                                                    <div class="form-floating mb-3">
                                                        <input 
                                                            name="longitude"
                                                            type="text" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            value="<?= $location->long ?>"
                                                            required
                                                        >
                                                        <label for="floatingInput">
                                                            Zemepisná dĺžka (long)
                                                        </label>
                                                    </div>
                                                    <div class="form-floating mb-3">
                                                        <input 
                                                            name="date"
                                                            type="date" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            value="<?= $location->date ?>"
                                                            required
                                                        >
                                                        <label for="floatingInput">
                                                            Dátum
                                                        </label>
                                                    </div>
                                                    <div class="bg-white border rounded p-2 mb-0">
                                                        <label for="floatingInput" class="image-upload-title">
                                                            Titulná fotka
                                                        </label>
                                                        <input 
                                                            name="image"
                                                            type="file" 
                                                            class="form-control" 
                                                            id="floatingInput" 
                                                            autocomplete="off"
                                                        >
                                                    </div>
                                                    <div class="card-footer card-footer-accordeon">
                                                        <button class="btn btn-success me-2">
                                                            Uložiť zmeny
                                                        </button>
                                                        <?php if(isset($_GET['vylet'])) { ?>
                                                            <a class="text-black text-decoration-none" href="/edit">Všetky výlety</a>
                                                         <?php } ?>
                                                        <a 
                                                            href="/edit/<?= $key ?>/delete" 
                                                            class="btn float-end text-danger ps-0 pe-0"
                                                            style="outline:none!important;box-shadow:none!important;border:none!important;"
                                                            onclick="if(!confirm('Určite?')) { return false }"
                                                        >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-6 col-lg-8">
                    <div class="card rounded-0">
                        <div class="card-header p-3 bg-white">
                            Výlety
                        </div>
                        <div class="card-body">
                            Zatiaľ žiaden výlet
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
    <script src="js/search.js" ></script>
    <script src="js/tooltips.js" ></script>
</body>
</html>