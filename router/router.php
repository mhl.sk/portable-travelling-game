<?php 

    class Router {

        private $router;

        public function __construct()
        {
            $this->controller();
            $this->router();
            $this->routes();
            $this->run();
        }

        private function controller()
        {
            $this->controller = new Controller();
        }

        private function router()
        {
            $this->router = new \Bramus\Router\Router();
        }

        private function routes()
        {
            $this->router->get('/', function() { $this->controller->index(); });
            $this->router->get('/edit', function() { $this->controller->edit(); });
            $this->router->get('/edit/{id}/delete', function($id) { $this->controller->delete($id); });
            $this->router->post('/add', function() { $this->controller->add($_POST, $_FILES); });
            $this->router->post('/edit/{id}', function($id) { $this->controller->update($id, $_POST, $_FILES); });
        }

        private function run()
        {
            $this->router->run();
        }

    } new Router();    

?>