<?php 

    use Ramsey\Uuid\Uuid;

    class Controller {

        public function index()
        {
            require __DIR__ . "/../views/index.php";
        }

        public function edit()
        {
            $locations = json_decode(file_get_contents(__DIR__ . "/../public/locations.json"));
            require __DIR__ . "/../views/edit.php";
        }

        public function delete($id)
        {
            $locations = json_decode(file_get_contents(__DIR__ . "/../public/locations.json"));
            foreach ($locations as $key => $location) {
                if($key == $id) {
                    unlink(__DIR__ . "/../public/" . $location->image);
                    unset($locations[$key]);
                }
            }
            $locations = array_values($locations);
            $locations = json_encode($locations);
            file_put_contents(__DIR__ . "/../public/locations.json", $locations);
            header("location:/edit");
        }

        public function add($data, $image)
        {
            $locations = json_decode(file_get_contents(__DIR__ . "/../public/locations.json"), true);
            if(!$locations) {
                $locations = [];
            }
            array_push($locations, [
                "name" => $data['name'],
                "image" => $this->upload($image),
                "lat" => $data['latitude'],
                "long" => $data['longitude'],
                "date" => $data['date'],
                "album" => $data['album']
            ]);
            usort($locations, [$this, "compare_date"]);
            $locations = array_values($locations);
            $locations = json_encode($locations);
            file_put_contents(__DIR__ . "/../public/locations.json", $locations);
            header("location:/edit");
        }

        private function compare_date($a, $b)
        {
            return strnatcmp($b['date'], $a['date']);
        }

        public function update($id, $data, $photo)
        {
            $locations = json_decode(file_get_contents(__DIR__ . "/../public/locations.json"), true);
            foreach ($locations as $key => $location) {
                if($key == $id) {
                    $locations[$key]["name"] = $data['name'];
                    if(isset($photo) && isset($photo['image']['name']) && $photo['image']['name']) {
                        unlink(__DIR__ . "/../public/" . $locations[$key]["image"]);
                        $locations[$key]["image"] = $this->upload($photo);
                    }
                    $locations[$key]["lat"] = $data['latitude'];
                    $locations[$key]["long"] = $data['longitude'];
                    $locations[$key]["date"] = $data['date'];
                    $locations[$key]["album"] = $data['album'];
                }
            }
            usort($locations, [$this, "compare_date"]);
            $locations = array_values($locations);
            $locations = json_encode($locations);
            file_put_contents(__DIR__ . "/../public/locations.json", $locations);
            header("location:/edit");
        }

        public function upload($data)
        {
            $errors = [];
            $extensions = ["jpeg","jpg","png"];
            $file_name = $data['image']['name'];
            $file_size = $data['image']['size'];
            $file_tmp = $data['image']['tmp_name'];
            $file_type = $data['image']['type'];
            $file_ext = strtolower(@end(explode('.', $file_name)));
            if(in_array($file_ext, $extensions) === false){
                die("Obrázok nespĺňa pravidlá. Uprav rozlíšenie na 1280x720px a veľkosť drž pod 5MB");
            } else {
                $uuid = Uuid::uuid7()->toString();
                move_uploaded_file($file_tmp, "covers/" . $uuid . "." . $file_ext);
                return $image = "/covers/" . $uuid . "." . $file_ext;
            }
        }

    }

?>