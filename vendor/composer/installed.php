<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'bramus/router' => array(
            'pretty_version' => '1.6.1',
            'version' => '1.6.1.0',
            'reference' => '55657b76da8a0a509250fb55b9dd24e1aa237eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bramus/router',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'brick/math' => array(
            'pretty_version' => '0.11.0',
            'version' => '0.11.0.0',
            'reference' => '0ad82ce168c82ba30d1c01ec86116ab52f589478',
            'type' => 'library',
            'install_path' => __DIR__ . '/../brick/math',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'gumlet/php-image-resize' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'reference' => 'aee4e2573e83476ffd0ac64b7269f838c8cd9d68',
            'type' => 'library',
            'install_path' => __DIR__ . '/../gumlet/php-image-resize',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'larapack/dd' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => '561b5111a13d0094b59b5c81b1572489485fb948',
            'type' => 'package',
            'install_path' => __DIR__ . '/../larapack/dd',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ramsey/collection' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'a4b48764bfbb8f3a6a4d1aeb1a35bb5e9ecac4a5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/collection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '4.7.4',
            'version' => '4.7.4.0',
            'reference' => '60a4c63ab724854332900504274f6150ff26d286',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '4.7.4',
            ),
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v6.2.11',
            'version' => '6.2.11.0',
            'reference' => '7d10f2a5a452bda385692fc7d38cd6eccfebe756',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
