if(document.getElementById("phrase")) {
    "keyup change".split(" ").forEach(function(eventname){
        document.getElementById("phrase").addEventListener(eventname, function(e){
            var phrase = e.target.value.toLowerCase();
            var trips = Array.from(document.getElementsByClassName("trip"));
            if(phrase.length) {
                trips.forEach(function(trip){
                    Array.from(trip.getElementsByTagName("button")).forEach(function(button){
                        if(button.classList.contains("accordion-button")){
                            if(!button.classList.contains("collapsed")) {
                                button.dispatchEvent(new Event('click'));
                            }
                        }
                    });
                    trip.style.display = "none";
                    if(trip.getAttribute("search").includes(phrase)){
                        trip.style.display = "block";
                    }
                });
            } else {
                trips.forEach(function(trip){
                    trip.style.display = "block";
                    Array.from(trip.getElementsByTagName("button")).forEach(function(button){
                        if(button.classList.contains("accordion-button")){
                            if(!button.classList.contains("collapsed")) {
                                button.dispatchEvent(new Event('click'));
                            }
                        }
                    });
                });

            }
        });
    });
}

var url = new URL(window.location.href);
var vylet = url.searchParams.get("vylet");
var datum = url.searchParams.get("datum");
if(vylet){
    var trips = Array.from(document.getElementsByClassName("trip"));
    document.getElementById("phrase").value = vylet;
    document.getElementById("phrase").dispatchEvent(new Event('keyup'));
    trips.forEach(function(trip){
        if(trip.style.display != "none") {
            if(!trip.getAttribute("search").includes(datum)) {
                trip.style.display = "none";
            }
            Array.from(trip.getElementsByTagName("button")).forEach(function(button){
                if(button.classList.contains("accordion-button")){
                    button.dispatchEvent(new Event('click'));
                }
            });
        }
    });
    window.history.pushState({}, document.title, "/edit");
}