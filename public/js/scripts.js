/** Set starting location */
var map = L.map('map').setView([48.705, 19.578], 8);

/** Initiate map */
var tiles = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png').addTo(map);

/** Set icon settings */
var LeafIcon = L.Icon.extend({
    options: {
       iconSize: [20, 20],
       popupAnchor: [0, -15]
    }
});

/** Create visited icon */
var visitedIcon = new LeafIcon({
    iconUrl: '/img/visited.png'
});

/** Fetch locations */
var locations = fetch("locations.json?time=" + Date.now()).then(response => response.json()).then(data => {
  
    data.forEach(function(location){
        location.date = location.date.split("-").reverse().join(".");
        L.marker(
            [
                location.lat,
                location.long
            ], 
            {
                icon: visitedIcon
            }
        ).addTo(map).bindPopup(`
            <img class="location-image" src="` + location.image + `">
            <h2 class="location-title">
                <b>` + location.name + `</b>
            </h2>
            <p>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                    <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
                    <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                </svg>
                Boli sme tu ` + location.date + `
            </p>
            <br>
            <a href="` + location.album + `" target="_blank" style="padding-bottom: 0px">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-camera" viewBox="0 0 16 16">
                    <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z"/>
                    <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
                </svg>
                Otvor fotoalbum
            </a>
            <div>
                <a href="/edit?vylet=` + location.name + `&datum=` + location.date + `" class="float-end edit-event">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                    Upraviť výlet
                </a>
            </div>
        `);
    });
    Array.from(document.getElementsByClassName("leaflet-marker-icon")).forEach(function(marker) {
        marker.classList.add(marker.getAttribute("src") == "/img/not_visited.png" ? "icon-not-visited" : "icon-visited");
    });
});  


map.on('click', function(e) {
    if (window.event.ctrlKey) {
        navigator.clipboard.writeText(e.latlng.lat + ", " + e.latlng.lng);
        Array.from(document.getElementsByClassName('leaflet-grab')).forEach(function(grab) {
            grab.style.cursor = 'grab';
        });
        alert("Zemepisná šírka a dĺžka boli uložené do clipboardu");
    }
});

document.getElementsByTagName("body")[0].addEventListener('keydown', function (event) {
    if (event.ctrlKey) {
        Array.from(document.getElementsByClassName('leaflet-grab')).forEach(function(grab) {
            grab.style.cursor = 'crosshair';
        });
    }
});

document.getElementsByTagName("body")[0].addEventListener('keyup', function (event) {
    Array.from(document.getElementsByClassName('leaflet-grab')).forEach(function(grab) {
        grab.style.cursor = 'grab';
    });
});